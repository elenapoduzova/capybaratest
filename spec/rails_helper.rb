require 'capybara/rspec'
require 'webdrivers' 
require 'selenium-webdriver'
require 'allure-rspec'

Capybara.register_driver :chrome_headless do |app|
  options = Selenium::WebDriver::Chrome::Options.new
  options.add_argument('--headless')
  options.add_argument('--no-sandbox')
  options.add_argument('--disable-dev-shm-usage')
  options.add_argument('--disable-gpu')
  options.add_argument('--window-size=1400,1400')
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
end

Capybara.default_host = 'https://the-internet.herokuapp.com'

Capybara.configure do |config|
  config.default_max_wait_time = 5 # seconds
end

Capybara.default_driver = :selenium_chrome

RSpec.configure do |c|
  c.include AllureRSpec::Adaptor
end

AllureRSpec.configure do |c|
  c.output_dir = "log"
  c.clean_dir = false
end