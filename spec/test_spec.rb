require 'rails_helper'

feature "User signup" do
	background do
		page.visit ('https://the-internet.herokuapp.com/login')
	end
	
	describe 'Open login page', type: :feature do
	  scenario 'index page' do
		expect(page).to have_content('This is where you can log into the secure area.')
	  end
	end 

	describe 'Fill correct email and password', type: :feature do
	  scenario 'fill inputs' do
		within("#content") do
			page.fill_in "username", :with => "tomsmith"
			page.fill_in "password", :with => "SuperSecretPassword!"
			page.click_button "Login"
		end
		expect(page).to have_content('Welcome to the Secure Area. When you are done click logout below.')
		page.save_screenshot('screens/screenshot1.png')
	  end
	end
	
	describe 'Fill invalid email and password', type: :feature do
	  scenario 'fill inputs' do
		within("#content") do
			page.fill_in "username", :with => "test"
			page.fill_in "password", :with => "qwerty"
			page.click_button "Login"
		end
		expect(page).to have_css("#flash", :text =>"Your username is invalid!")
		page.save_screenshot('screens/screenshot2.png')
	  end
	end
	
	describe 'Check selenium link', type: :feature do
	  scenario 'check link' do
		within("#page-footer") do
			expect(page).to have_link("Elemental Selenium", :href=>"http://elementalselenium.com/")
		end
		new_window = window_opened_by { page.click_link 'Elemental Selenium' }
		within_window new_window do
			expect(page).to have_current_path('http://elementalselenium.com/')
			page.save_screenshot('screens/screenshot3.png')
		end
	  end
	end
end